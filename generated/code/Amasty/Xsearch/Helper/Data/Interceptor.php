<?php
namespace Amasty\Xsearch\Helper\Data;

/**
 * Interceptor class for @see \Amasty\Xsearch\Helper\Data
 */
class Interceptor extends \Amasty\Xsearch\Helper\Data implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Catalog\Model\Config $configAttribute, \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection $collection, \Magento\Search\Helper\Data $searchHelper, \Magento\Framework\Filter\StripTags $stripTags, \Magento\Framework\App\Helper\Context $context, \Magento\Customer\Model\SessionFactory $sessionFactory)
    {
        $this->___init();
        parent::__construct($configAttribute, $collection, $searchHelper, $stripTags, $context, $sessionFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleConfig($path)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getModuleConfig');
        if (!$pluginInfo) {
            return parent::getModuleConfig($path);
        } else {
            return $this->___callPlugins('getModuleConfig', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isIndexEnable($block = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isIndexEnable');
        if (!$pluginInfo) {
            return parent::isIndexEnable($block);
        } else {
            return $this->___callPlugins('isIndexEnable', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function highlight($text, $query)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'highlight');
        if (!$pluginInfo) {
            return parent::highlight($text, $query);
        } else {
            return $this->___callPlugins('highlight', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle(string $tabType)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getTabTitle');
        if (!$pluginInfo) {
            return parent::getTabTitle($tabType);
        } else {
            return $this->___callPlugins('getTabTitle', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBlocksHtml(\Magento\Framework\View\Layout $layout)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBlocksHtml');
        if (!$pluginInfo) {
            return parent::getBlocksHtml($layout);
        } else {
            return $this->___callPlugins('getBlocksHtml', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProductAttributes($requiredData = '')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProductAttributes');
        if (!$pluginInfo) {
            return parent::getProductAttributes($requiredData);
        } else {
            return $this->___callPlugins('getProductAttributes', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isSingleProductRedirect()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isSingleProductRedirect');
        if (!$pluginInfo) {
            return parent::isSingleProductRedirect();
        } else {
            return $this->___callPlugins('isSingleProductRedirect', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getResultUrl($query = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getResultUrl');
        if (!$pluginInfo) {
            return parent::getResultUrl($query);
        } else {
            return $this->___callPlugins('getResultUrl', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isNoIndexFollowEnabled()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isNoIndexFollowEnabled');
        if (!$pluginInfo) {
            return parent::isNoIndexFollowEnabled();
        } else {
            return $this->___callPlugins('isNoIndexFollowEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isSeoUrlsEnabled()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isSeoUrlsEnabled');
        if (!$pluginInfo) {
            return parent::isSeoUrlsEnabled();
        } else {
            return $this->___callPlugins('isSeoUrlsEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSeoKey()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSeoKey');
        if (!$pluginInfo) {
            return parent::getSeoKey();
        } else {
            return $this->___callPlugins('getSeoKey', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setStrippedQueryText($query, $engine = 'mysql')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setStrippedQueryText');
        if (!$pluginInfo) {
            return parent::setStrippedQueryText($query, $engine);
        } else {
            return $this->___callPlugins('setStrippedQueryText', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrentSearchEngineCode()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCurrentSearchEngineCode');
        if (!$pluginInfo) {
            return parent::getCurrentSearchEngineCode();
        } else {
            return $this->___callPlugins('getCurrentSearchEngineCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerGroupId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomerGroupId');
        if (!$pluginInfo) {
            return parent::getCustomerGroupId();
        } else {
            return $this->___callPlugins('getCustomerGroupId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isModuleOutputEnabled');
        if (!$pluginInfo) {
            return parent::isModuleOutputEnabled($moduleName);
        } else {
            return $this->___callPlugins('isModuleOutputEnabled', func_get_args(), $pluginInfo);
        }
    }
}
