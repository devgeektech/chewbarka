<?php
namespace Magento\Framework\Search\Request\Builder;

/**
 * Interceptor class for @see \Magento\Framework\Search\Request\Builder
 */
class Interceptor extends \Magento\Framework\Search\Request\Builder implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, \Magento\Framework\Search\Request\Config $config, \Magento\Framework\Search\Request\Binder $binder, \Magento\Framework\Search\Request\Cleaner $cleaner)
    {
        $this->___init();
        parent::__construct($objectManager, $config, $binder, $cleaner);
    }

    /**
     * {@inheritdoc}
     */
    public function setRequestName($requestName)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setRequestName');
        if (!$pluginInfo) {
            return parent::setRequestName($requestName);
        } else {
            return $this->___callPlugins('setRequestName', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setSize($size)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setSize');
        if (!$pluginInfo) {
            return parent::setSize($size);
        } else {
            return $this->___callPlugins('setSize', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setFrom($from)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setFrom');
        if (!$pluginInfo) {
            return parent::setFrom($from);
        } else {
            return $this->___callPlugins('setFrom', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setSort($sort)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setSort');
        if (!$pluginInfo) {
            return parent::setSort($sort);
        } else {
            return $this->___callPlugins('setSort', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function bindDimension($name, $value)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'bindDimension');
        if (!$pluginInfo) {
            return parent::bindDimension($name, $value);
        } else {
            return $this->___callPlugins('bindDimension', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function bind($placeholder, $value)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'bind');
        if (!$pluginInfo) {
            return parent::bind($placeholder, $value);
        } else {
            return $this->___callPlugins('bind', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'create');
        if (!$pluginInfo) {
            return parent::create();
        } else {
            return $this->___callPlugins('create', func_get_args(), $pluginInfo);
        }
    }
}
