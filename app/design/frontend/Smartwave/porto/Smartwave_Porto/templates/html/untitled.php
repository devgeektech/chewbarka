<div class="container-fluid">
  <div class="row">
    <div id="banner-slider-demo-8" class="owl-carousel owl-middle-narrow owl-banner-carousel">
      <div class="item" style="background:#f0f0f0;background-image:linear-gradient(#e8e8e8,#f0f0f0);position:relative;">
        <img src="{{media url="wysiwyg/smartwave/porto/homepage/08/slider/slide01n.jpg"}}" alt="" />
        <div class="container slider-content">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <p>test</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               <p>test</p>
            </div>
          </div>
        </div>
      </div>
      <div class="item" style="background:#f0f0f0;background-image:linear-gradient(#e8e8e8,#f0f0f0);position:relative;">
        <img src="{{media url="wysiwyg/smartwave/porto/homepage/08/slider/slide02n.jpg"}}" alt="" />
        <div class="container slider-content">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <p>test</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               <p>test</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      require([
        'jquery',
        'owl.carousel/owl.carousel.min'
      ], function ($) {
        var owl_8 = $("#banner-slider-demo-8").owlCarousel({
          items: 1,
          autoplay: true,
          autoplayTimeout: 5000,
          autoplayHoverPause: true,
          dots: false,
          navRewind: true,
          animateIn: 'fadeIn',
          animateOut: 'fadeOut',
          loop: true,
          nav: true,
          navText: ["<em class='porto-icon-chevron-left'></em>","<em class='porto-icon-chevron-right'></em>"]
        });
      });
    </script>
  </div>
</div>