<?php
namespace Cloudways\Mymodule\Model\Quote;

class Discount extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
	public function __construct(
	\Magento\Framework\Event\ManagerInterface $eventManager,
	\Magento\Store\Model\StoreManagerInterface $storeManager,
	\Magento\SalesRule\Model\Validator $validator,
	\Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
	) 
	{
		$this->setCode('testdiscount');
		$this->eventManager = $eventManager;
		$this->calculator = $validator;
		$this->storeManager = $storeManager;
		$this->priceCurrency = $priceCurrency;
	}
 
	public function collect(
	\Magento\Quote\Model\Quote $quote,
	\Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
	\Magento\Quote\Model\Quote\Address\Total $total
	)
	{
		parent::collect($quote, $shippingAssignment, $total);

		/*Custom Code here*/
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
		 
		$itemsCollection = $cart->getQuote()->getItemsCollection();
		
		$itemsVisible = $cart->getQuote()->getAllVisibleItems();
	 	
		$items = $cart->getQuote()->getAllItems();
		$i = 0;
		$final_discount = 0;
		/*foreach ($items as $items_item) {
			print_r($items_item->debug());
		}*/

		/*Item start here*/
		foreach($itemsVisible as $item) {
	 		$item_qty_options = $item->getOptions();
	 		if(!empty($item_qty_options)){
	 			foreach ($item_qty_options as $qty_optionskey => $qty_options_value) {
	 				if($qty_optionskey == 3){
	 					$product_id = $qty_options_value->getProduct()->getId();
	 				}else{
	 					$product_id = '';
	 				}
	 			}
	 		}		 	
			
			if(isset($product_id) && $product_id != ''){
			   	$_product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
				$attributeValue = $_product->getData('table_discounts');
				$attributeValue_new[$attributeValue]['regular_price'] = $_product['price'];
				$attributeValue_new[$attributeValue]['price'][] = $item['base_row_total'];
				$attributeValue_new[$attributeValue]['qty'][] = $item->getQty();
				$attributeValue_new[$attributeValue]['tier'] = $_product->getTierPrice();
			}        
		    $i++;
		}
		/*Item end here*/
		// print_r($attributeValue_new);
		/*Attribute custom data start here*/
		if(isset($attributeValue_new) && !empty($attributeValue_new)){
			foreach ($attributeValue_new as $attributeValue_new_key => $attributeValue_new_value) {
				$att_val_final[$attributeValue_new_key]['regular_price'] = $attributeValue_new_value['regular_price'];
				$att_val_final[$attributeValue_new_key]['qty'] = array_sum($attributeValue_new_value['qty']);
				$att_val_final[$attributeValue_new_key]['price'] = array_sum($attributeValue_new_value['price']);
				$att_val_final[$attributeValue_new_key]['tier'] = $attributeValue_new_value['tier'];
			}
			
			if(isset($att_val_final) && !empty($att_val_final)){
				foreach ($att_val_final as $att_val_finalkey => $att_val_finalvalue) {


					$inserted[0]['price'] = $att_val_finalvalue['regular_price'];
					$inserted[0]['price_qty'] = 0;
					array_splice( $att_val_finalvalue['tier'], 0, 0, $inserted ); 


					$k = 0;
					$ak = 1;
					// echo "Key = ".$att_val_finalkey."  ";
					// print_r($att_val_finalvalue);
					foreach ($att_val_finalvalue['tier'] as $tier_value) {						
						if($att_val_finalvalue['qty'] >= $k && (isset($att_val_finalvalue['tier'][$ak]['price_qty']) && $att_val_finalvalue['qty'] < $att_val_finalvalue['tier'][$ak]['price_qty'])){

							$k = $att_val_finalvalue['tier'][$ak]['price_qty'] + $k;
							$multiply_val = $tier_value['price']*$att_val_finalvalue['qty'];
							if( $multiply_val != $att_val_finalvalue['price']){
								$final_discount = $att_val_finalvalue['price'] - $multiply_val;
							}

						}
						$ak++;
					}

				}
			}
		}

		/*Attribute array end here*/
// 		echo "Final Discount = ".$final_discount;
		/*Custom Code here*/
		$address = $shippingAssignment->getShipping()->getAddress();
		$label = 'Discount Group';
		$TotalAmount=$total->getSubtotal();
		if($TotalAmount>0 && $TotalAmount > $final_discount){
			$TotalAmount=$TotalAmount - $final_discount; //Set 10% discount
		}else{
			$TotalAmount=$TotalAmount - 0; //Set 10% discount
		}

// 		echo "total Amout = ".$TotalAmount;
		$discountAmount ="-".$final_discount; 
		$appliedCartDiscount = 0;

		if($total->getDiscountDescription())
		{
			$appliedCartDiscount = $total->getDiscountAmount();
			$discountAmount = $total->getDiscountAmount()+$discountAmount;
			$label = $total->getDiscountDescription().', '.$label;
		} 

		$total->setDiscountDescription($label);
		$total->setDiscountAmount($discountAmount);
		$total->setBaseDiscountAmount($discountAmount);
		$total->setSubtotalWithDiscount($total->getSubtotal() + (float)$discountAmount);
		$total->setBaseSubtotalWithDiscount($total->getBaseSubtotal() + (float)$discountAmount);

		if(isset($appliedCartDiscount))
		{
			$total->addTotalAmount($this->getCode(), (float)$discountAmount - $appliedCartDiscount);
			$total->addBaseTotalAmount($this->getCode(), (float)$discountAmount - $appliedCartDiscount);
		} 
		else 
		{
			$total->addTotalAmount($this->getCode(), (float)$discountAmount);
			$total->addBaseTotalAmount($this->getCode(), (float)$discountAmount);
		}

// 		echo "-----------------------------------------------------";
		return $this;
	}
 
	public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
	{
		 $result = null;
		 $amount = $total->getDiscountAmount();
		 
		 if ($amount != 0)
		 { 
			 $description = $total->getDiscountDescription();
			 $result = [
			 'code' => $this->getCode(),
			 'title' => strlen($description) ? __('Discount (%1)', $description) : __('Discount'),
			 'value' => $amount
			 ];
		 }
		 return $result;
	}
}