<?php
/**
 * Created by PhpStorm.
 * User: Toan FrontEnd
 * Date: 7/30/2018
 * Time: 3:05 PM
 */

namespace Magenest\QuickBooksDesktop\Model\Config\Source\Queue;

class TypeQuery
{
    const QUERY_SYNC = 1;
    const QUERY_COMPANY = 2;
    const QUERY_TAX = 3;
}
