<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_QuickBooksDesktop extension
 * NOTICE OF LICENSE
 */

namespace Magenest\QuickBooksDesktop\Model\QBXML;

use Magento\Sales\Model\Order as OrderModel;
use Magenest\QuickBooksDesktop\Model\QBXML;
use Magenest\QuickBooksDesktop\Model\Mapping;
use Magenest\QuickBooksDesktop\Helper\CreateQueue as QueueHelper;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\ObjectManagerInterface;
use Magenest\QuickBooksDesktop\Model\Config\Source\Version;

/**
 * Class SalesOrder
 *
 * @package Magenest\QuickBooksDesktop\Model\QBXML
 */
class SalesOrder extends QBXML
{
    /**
     * @var QueueHelper
     */
    protected $_queueHelper;

    /**
     * @var OrderModel
     */
    protected $_order;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var Mapping
     */
    public $_map;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;

    /**
     * SalesOrder constructor.
     * @param OrderModel $order
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param Mapping $map
     * @param ProductFactory $productFactory
     * @param \Magento\Framework\App\ObjectManager $objectManager
     * @param QueueHelper $queueHelper
     */
    public function __construct(
        OrderModel $order,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        Mapping $map,
        ProductFactory $productFactory,
        ObjectManagerInterface $objectManager,
        QueueHelper $queueHelper
    ) {
        parent::__construct($objectManager);
        $this->_order = $order;
        $this->scopeConfig = $scopeConfig;
        $this->_map = $map;
        $this->_productFactory = $productFactory;
        $this->_queueHelper = $queueHelper;
        $this->_version = $this->_queueHelper->getQuickBooksVersion();
    }

    /**
     * Get XML using sync to QBD
     *
     * @param $id
     * @return string
     */
    public function getXml($id)
    {
        /** @var \Magento\Sales\Model\Order $model */
        $model = $this->_order->load($id);
        $billAddress = $model->getBillingAddress();
        $shipAddress = $model->getShippingAddress();
        if ($model->getCustomerId()) {
            $customerReceive = $model->getCustomerName() .' '. $model->getCustomerId();
        } else {
            $customerReceive = $billAddress->getName() .' '. $model->getIncrementId();
        }

        $xml = $this->multipleXml($customerReceive, ['CustomerRef', 'FullName']);
        $xml .= $this->simpleXml($model->getIncrementId(), 'RefNumber');
        $xml .= $billAddress ? $this->getAddress($billAddress) : '';
        $xml .= $shipAddress ? $this->getAddress($shipAddress, 'ship') : '';
        $shipMethod = strtok($model->getShippingMethod(), '_');

        if (!empty($model->getShippingMethod())) {
            $xml .= $this->multipleXml($shipMethod, ['ShipMethodRef', 'FullName']);
        }

        $taxCode = $this->getTaxCodeTransaction($model);
        if ($taxCode != null && $this->_version == Version::VERSION_US) {
            $xml .= $this->multipleXml($taxCode, ['ItemSalesTaxRef', 'FullName']);
        }

        /** @var \Magento\Sales\Model\Order\Item $item */
        foreach ($model->getAllItems() as $item) {
            $xml .= $this->getOrderItem($item);
        }

        if ($model->getShippingAmount() != 0) {
            $dataShipping =
                [
                    'type' => 'Shipping',
                    'desc' => $model->getShippingDescription(),
                    'rate' => $model->getShippingAmount(),
                    'tax_amount' => $model->getShippingTaxAmount(),
                    'txn_id' => null,
                    'taxcode' => $taxCode
                ];

            $xml .= $this->getOtherItem($dataShipping, 'SalesOrderLineAdd');
        }

        if ($model->getDiscountAmount() != 0) {
            $discount = $model->getDiscountAmount();
            if ($discount < 0) {
                $discount = 0 - $discount;
            }
            $dataDiscount =
                [
                    'type' => 'Discount',
                    'desc' => $model->getDiscountDescription(),
                    'rate' => $discount,
                    'tax_amount' => $model->getDiscountTaxCompensationAmount(),
                    'txn_id' => null,
                    'taxcode' => $taxCode
                ];
            $xml .= $this->getOtherItem($dataDiscount, 'SalesOrderLineAdd');
        }

        return $xml;
    }

    /**
     * Get Order Item XML
     *
     * @param \Magento\Sales\Model\Order\Item $item *
     * @return string
     */
    protected function getOrderItem($item)
    {
        $price = $item->getPrice();
        $taxAmount = $item->getTaxAmount();

        $qty = $item->getQtyOrdered();
        $sku = $item->getSku();

        if ($sku) {
            if ($qty > 0 && $price > 0) {
                $hasTax = false;
                $taxCode = $this->getTaxCodeItem($item->getItemId());
                $xml = '<SalesOrderLineAdd>';
                $xml .= $this->multipleXml(substr(str_replace(['&', '”', '\'', '<', '>', '"'], ' ', $sku), 0, 30), ['ItemRef', 'FullName']);
                $xml .= $this->simpleXml($qty, 'Quantity');
                $xml .= $this->simpleXml($price, 'Rate');

                if ($taxAmount != 0) {
                    $hasTax = true;
                }
                $xml .= $this->getXmlTax($taxCode, $hasTax);
                $xml .= '</SalesOrderLineAdd>';
            }
        } else {
            $xml = '<SalesOrderLineAdd>';
            $xml .= $this->multipleXml('Not Found Product From M2', ['ItemRef', 'FullName']);
            $xml .= '</SalesOrderLineAdd>';
            return $xml;
        }

        return $xml;
    }
}
