$(document).ready(function () {
    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.on('click', function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='checkbox']"),
            skipCheckbox = $("#skip-" + curStepBtn);
            isValid = true;

        $(".form-group").removeClass("has-error");
        if (curInputs.length) {
            for(var i=0; i<curInputs.length; i++){
                if (!curInputs[i].validity.valid){
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }
        }

        if ((skipCheckbox).prop('checked') == true) {
            isValid = true;
            nextStepWizard.removeAttr('disabled').trigger('click');
        } else {
            if (isValid) {
                var $this = $(this);
                $this.button('loading');
                var ajaxRequest = curStepBtn + '.php';
                var ajaxParams = {};

                switch (curStepBtn) {
                    case 'step-2':
                        if (curInputs.length) {
                            var modulesEnable = [];
                            var modulesDisable = [];
                            for (var i = 0; i < curInputs.length; i++) {
                                if (!$(curInputs[i]).prop('checked')) {
                                    var inputVal = $(curInputs[i]).val();
                                    modulesDisable.push(inputVal);
                                } else {
                                    var inputVal = $(curInputs[i]).val();
                                    modulesEnable.push(inputVal);
                                }
                            }
                        }
                        ajaxParams.modulesEnable = modulesEnable;
                        ajaxParams.modulesDisable = modulesDisable;

                        break;
                    case 'step-3':
                        ajaxParams.deleteInstaller = $('#delete-installer').val();
                }

                $.ajax({
                    type: "POST",
                    url: ajaxRequest,
                    dataType: 'json',
                    data: ajaxParams,
                    success: function(data) {
                        $this.button('reset');
                        if (!data.error) {
                            nextStepWizard.removeAttr('disabled').trigger('click');
                            $('.result-container').append("<p class='success'>" + curStepBtn.toUpperCase() + ": <br/> " + data.msg + "</p>");

                            if (typeof data.modules != 'undefined' && data.modules.length) {
                                $('#modules_list').html('');
                                data.modules.forEach(function(el) {
                                    var checked = el.active == '1' || el.isNew == '1' ? 'checked="checked"' : '';
                                    var disabled = el.selectable == '1' ? 'disabled="disabled"' : '';
                                    var required = el.selectable == '1' ? ' (mandatory) ' : '';
                                    var requiredClass = el.selectable == '1' ? ' mandatory' : '';
                                    var isNew = el.isNew == '1' && data.allNew != '1' ? '(new)' : '';
                                    var isNewClass = el.isNew == '1' && data.allNew != '1' ? 'new-module' : '';
                                    var html ='<li clss="form-group">';
                                    html +='<fieldset>';
                                    html += '<input id="' + el.value.toLocaleLowerCase() + '" type="checkbox" name="' + el.value.toLocaleLowerCase() + '" value="' + el.value + '" ' + checked + ' ' + disabled + ' />';
                                    html += '<label class="' + isNewClass + requiredClass + '" for="' + el.value.toLocaleLowerCase() + '">' + el.name + ' ' + required + isNew + '</label>';
                                    html += '</fieldset>';
                                    html += '</li>';

                                    $('#modules_list').append(html);
                                });
                            }
                        } else {
                            $('.result-container').append("<p class='error'>" + curStepBtn.toUpperCase() + ": <br/> "  + data.msg + "</p>");
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        $('.result-container').append("<p class='error'>" + curStepBtn.toUpperCase() + ": <br/> " + " Server request error: " + errorThrown + "</p>");
                        $this.button('reset');
                    }
                });
            }
        }

    });

    $('div.setup-panel div a.btn-primary').trigger('click');

    /** remove notification message */
    $('.btn-proc-1').on('click', function() {
       $('.notification-msg').hide();
    });
    $('.step-1-link').on('click', function() {
        $('.notification-msg').show();
    });

});
